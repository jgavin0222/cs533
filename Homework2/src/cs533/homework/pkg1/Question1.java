package cs533.homework.pkg1;
import java.util.Scanner;
import java.text.DecimalFormat;

/**
 * @author Jack Gavin
 * Written for CS533
 */
public class Question1 {

    private static DecimalFormat df = new DecimalFormat("#.####");
    public static void main(String[] args) {
        System.out.print("Enter a radius: ");
        
        Scanner scanner = new Scanner(System.in);
        int radius = scanner.nextInt();
        
        System.out.println("Values are rounded to four decimal places");
        System.out.println("Diameter: "+df.format(2 * radius));
        System.out.println("Circumference: "+df.format(2 * Math.PI * radius));
        System.out.println("Area: "+df.format(Math.PI * (Math.pow(radius, 2))));
    }
}

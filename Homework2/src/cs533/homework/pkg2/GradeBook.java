package cs533.homework.pkg2;

/**
 * @author Jsck Gavin
 * Written for CS533
 */
public class GradeBook {
    private String courseName;
    private String instructorName;
    
    public GradeBook(String name, String instructor)
    {
        this.courseName = name;
        this.instructorName = instructor;
    }
    
    public void setCourseName(String name)
    {
        this.courseName = name;
    }
    
    public void setInstructorName(String instructor)
    {
        this.instructorName = instructor;
    }
    
    public String getCourseName()
    {
        return this.courseName;
    }
    
    public String getInstructorName()
    {
        return this.instructorName;
    }
    
    public void displayMessage()
    {
        System.out.printf("Welcome to the grade book for \n%s!\nThis course is presented by: %s\n", getCourseName(), getInstructorName());
    }
}

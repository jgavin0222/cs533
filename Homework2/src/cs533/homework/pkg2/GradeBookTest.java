package cs533.homework.pkg2;

/**
 * @author Jack Gavin
 * Written for CS533
 */
public class GradeBookTest {
    public static void main(String[] args)
    {
        GradeBook gradeBook1 = new GradeBook("CS101 Introduction to Java Programming", "Dr. Andy Liu");
        GradeBook gradeBook2 = new GradeBook("CS102 Data Structures in Java", "Dr. George Li");
    
        System.out.printf("gradeBook1 course name is: %s\n", gradeBook1.getCourseName());
        System.out.printf("gradeBook2 course name is: %s\n", gradeBook2.getCourseName());
        
        System.out.printf("gradeBook1 instructor name is: %s\n", gradeBook1.getInstructorName());
        System.out.printf("gradeBook2 instructor name is: %s\n", gradeBook2.getInstructorName());
        
        gradeBook1.setInstructorName("Overridden Instructor Value - 1");
        gradeBook2.setInstructorName("Overridden Instructor Value - 2");
        
        gradeBook1.displayMessage();
        gradeBook2.displayMessage();
    }
}

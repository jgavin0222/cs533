/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs533.homework.pkg1;

/**
 *
 * @author Jack Gavin
 */
public class Rectangle extends Parallelogram {
    private final Point p1, p2, p3, p4;
    
    public Rectangle(Point p1, Point p2, Point p3, Point p4)
    {
        super(p1, p2, p3, p4);
        
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }
    
    private double getLength()
    {
        return Quadrilateral.getDistance(p1, p2);
    }
    
    private double getHeight()
    {
        return Quadrilateral.getDistance(p1, p3);
    }
    
    @Override
    public double getArea()
    {
        double l = this.getLength();
        double h = this.getHeight();
        
        // Formula l * h
        return l * h;
    }
    
    @Override
    public double getPerimeter()
    {  
        // Formula (2 * h) + (2 * l)
        return ((2 * this.getHeight()) + (2 * this.getLength()));
    }
}

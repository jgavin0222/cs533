/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs533.homework.pkg1;

/**
 *
 * @author Jack Gavin
 */
public class Trapezoid extends Quadrilateral {
    private final Point p1, p2, p3, p4;
    
    public Trapezoid(Point p1, Point p2, Point p3, Point p4)
    {
        super(p1, p2, p3, p4);
        
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }
    
    @Override
    public double getArea()
    {
        double h = Math.abs(p1.getY() - p3.getY());
        double a = Quadrilateral.getDistance(p1, p2);
        double b = Quadrilateral.getDistance(p3, p4);
        
        // Formula is ((a + b) / 2) * h
        return ((a + b) / 2) * h;
    }
    
    @Override
    public double getPerimeter()
    {
        double a = Quadrilateral.getDistance(p1, p2);
        double b = Quadrilateral.getDistance(p3, p4);
        double c = Quadrilateral.getDistance(p3, p1);
        double d = Quadrilateral.getDistance(p2, p4);
        
        // Formula is (a + b + c + d)
        return (a + b + c +d);
    }
}

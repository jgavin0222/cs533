/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs533.homework.pkg1;

/**
 *
 * @author Jack Gavin
 */
public class Square extends Rectangle {
    private final Point p1, p2, p3, p4;
    
    public Square(Point p1, Point p2, Point p3, Point p4)
    {
        super(p1, p2, p3, p4);
        
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }
    
    private double getSideLength()
    {
        return Quadrilateral.getDistance(p1, p2);
    }
    
    @Override
    public double getArea()
    {
        // Formula s^2
        return Math.pow(this.getSideLength() ,2);
    }
    
    @Override
    public double getPerimeter()
    {
        // Forumla 4 * s
        return (4 * this.getSideLength());
    }
}

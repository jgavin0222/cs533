/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs533.homework.pkg1;

/**
 *
 * @author Jack Gavin
 */
public class CS533Homework3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Point p1 = new Point(1, 15);
        Point p2 = new Point(12, 15);
        Point p3 = new Point(0, 0);
        Point p4 = new Point(13, 0);
        
        Trapezoid t1 = new Trapezoid(p1, p2, p3, p4);
        System.out.println("The area of the trapezoid is: "+t1.getArea());
        
        p1.setPoint(2, 10);
        p2.setPoint(11, 10);
        p3.setPoint(1, 1);
        p4.setPoint(10, 1);

        Parallelogram px = new Parallelogram(p1, p2, p3, p4);
        System.out.println("The area of the parallelogram is: "+px.getArea());
        
        p1.setPoint(0, 3);
        p2.setPoint(10, 3);
        p3.setPoint(0,0);
        p4.setPoint(10, 0);
        
        Rectangle r1 = new Rectangle(p1, p2, p3, p4);
        System.out.println("The area of the rectangle is: "+r1.getArea());
        
        p1.setPoint(0, 3);
        p2.setPoint(3, 3);
        p3.setPoint(0, 0);
        p4.setPoint(3, 0);
        
        Square s1 = new Square(p1, p2, p3, p4);
        System.out.println("The area of the square is: "+s1.getArea());
    }   
}

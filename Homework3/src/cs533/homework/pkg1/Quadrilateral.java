/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs533.homework.pkg1;

/**
 *
 * @author Jack Gavin
 */
public abstract class Quadrilateral {
    private Point p1, p2, p3, p4;
    
    Quadrilateral(Point p1, Point p2, Point p3, Point p4)
    {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }
    
    public abstract double getArea();
    public abstract double getPerimeter();

    public static double getDistance(Point p1, Point p2)
    {
        return Math.abs(Math.sqrt(Math.pow(p2.getX() - p1.getX(),2)+Math.pow(p2.getY()-p1.getY(),2)));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs533.homework.pkg1;

/**
 *
 * @author Jack Gavin
 */
public class Parallelogram extends Trapezoid {
    private final Point p1, p2, p3, p4;
    
    public Parallelogram(Point p1, Point p2, Point p3, Point p4)
    {
        super(p1, p2, p3, p4);
        
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }
    
    @Override
    public double getArea()
    {
        double b = Quadrilateral.getDistance(p1, p2);
        double h = Math.abs(p1.getY() - p3.getY());
        
        // Formula b * h
        return (b * h);
    }
    
    @Override
    public double getPerimeter()
    {
        double a = Quadrilateral.getDistance(p3, p1);
        double b = Quadrilateral.getDistance(p1, p2);
        
        // Formula 2(a + b)
        return (2 * (a + b));
    }
}
